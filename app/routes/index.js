const express = require('express');
const router = express.Router();
const path = require('path');
const { loginRequired, login } = require('../controller/user');

//Definimos la ruta donde encontrar los controladores usando la libreria Path
var ctrlDir = path.resolve("controller/");
// importamos el controllador que queremos utilizar
var userController = require(path.join(ctrlDir, "user"));

router.get('/chatList', function (req, res) {
    res.sendFile(path.resolve('views/chatList.html'));
});

router.get('/history/list', function (req, res) {
    res.sendFile(path.resolve('views/history.html'));
});

router.post('/chatView', function (req, res) {
    res.sendFile(path.resolve('views/chatView.html'));
});

router.get('/history/view', function (req, res) {
    res.sendFile(path.resolve('views/historyView.html'))
});

router.get('/register', function(req, res) {
    res.sendFile(path.resolve('views/register.html'));
});

// routes/index.js

router.get('/', function(req,res) {
    res.sendFile(path.resolve('views/index.html'));
});

//router.get('/chatSelect', loginRequired, userController.chatSelected);

//Link routes and functions
// user routes

router.get('/users', userController.list);
router.get('/user/:id', userController.find);
router.get('/logout', loginRequired ,userController.logout);
router.post('/user/login', userController.login);
router.post('/user/register', userController.add);

module.exports = router;