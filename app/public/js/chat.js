

document.addEventListener("DOMContentLoaded", function (event) {
    //Nos conectamos al servicio de socket
    const socket = io("http://10.200.242.225:2000");
    const userMessage = document.getElementById("userMessage");
    const chatBox = document.getElementById("chatText");
    let msgInput;
    /*
    * Acciones que se realizan cuando se establece conexión con el servidor de socket.
    * tambien introducimos al usuario en la sala que corresponde
    */
    socket.on("connected", (data) => {
        console.log(data.msg);
    });

    /*
     * Acciones que se realizarán cuando otro usuario envia mensaje
     */
    socket.on("toChat", (data) => {
        toSend = { user: data.msg.user, text: data.msg.text };
        console.log(data);
        chatBox.append(toSend.user + ": " + toSend.text + "\n");
        
        // console.log(data.user);
        // console.log(data.msg);
    });

    /**
     * Acciones cuando se pulsa el botón de "Enviar"
     */
    document.getElementById("send").addEventListener("submit", (e) => {
        e.preventDefault();
        msgInput = userMessage.value;
        
        // deixem el container per escriure buit
        userMessage.value = "";
        let msg = msgInput;
        
        
        //Mostramos el mensaje en la ventana para el usuario que lo enviau
        var toSend = { user: "Yo", text: msg };
        chatBox.append(toSend.user + ": " + toSend.text +"\n");
        // Definimos el mensaje que vamos a enviar
        socket.emit("broadcast", toSend);
    });
});
