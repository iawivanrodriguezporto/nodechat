//controllers/user.js

var User = require('../models/user');
const path1 = require('path');

const path = __dirname + '/views/';

//Create a new user and save it
var add = (req, res) => {
	var user = new User({ username: req.body.username, password: req.body.password });
	user.save();
	console.log(user);
	res.redirect('/');
	return user;
};

//find all people
var list = (req, res, next) => {
	User.find(function (err, users) {
		return users;
	});
};

//find person by id
var find = (req, res) => {
	User.findOne({ _id: req.params.id }, function (error, user) {
		return user;
	})
};

var logout = (req, res) => {
	delete req.session.userId;
	res.redirect('/');
	res.json({message: 'loged out'});
}

var login = async (req, res) => {
	const user = await User.findOne({username: req.body.username});
	if (!user) {
		return res.status(404).json({message: 'username does not exist'});
	}
	if (user.password != req.body.password) {
		return res.status(401).json({message: 'password incorrect'});
	}
	req.session.userId = user.id;
	//res.json({message: 'loged in', username: user.username, password: user.password});
    res.sendFile(path1.resolve('views/chatList.html'));
}

var loginRequired = async (req, res, next) => {
	if (!req.session || !req.session.userId) {
		return res.status(403).json({message: 'you must login'});
	}
	req.user = await User.findById(req.session.userId);
	if (!req.user) {
		return res.status(403).json({message: 'this user id no longer exists'});
	}
	next();
}

var chatSelected = (req, res) => {
	
	res.sendFile(path1.resolve('views/chatView.html'));
}

module.exports = {
	add,
	list,
	find,
	logout,
	login,
	loginRequired,
	chatSelected
}