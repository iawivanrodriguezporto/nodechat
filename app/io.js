// require("dotenv").config();
// const socketPort = process.env.SOCKET_PORT || 2000;

//io.js
const socketPort =  2000;
const session = require("express-session");
const { Server } = require("socket.io");
const socketServer = require("http").createServer();

socketServer.listen(socketPort, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${socketPort}`);
});
const io = new Server(socketServer, {
	cors: {
	  origin: "http://10.200.242.225:3000", //Esta será la dirección de vuestra web
	},
  });
  io.on("connection", (socket) => {
	console.log("Nuevo cliente");
	socket.emit("connected", {
	  msg: "Bienvenido al chat de Ivan",
	});
	//Este socket tendrá un atributzo user con valor Pedro,
	//desde la primera conexión hasta que se cierre el socket
	if (!socket.user) socket.user = "Ivan";
  
	//Todos los clientes que estén escuchando el evento "toChat" recibiran el mensaje enviado por el cliente que lanzó el mensaje
	socket.on("broadcast", (data) => {
	  socket.broadcast.emit("toChat", {msg: data});
	});
  });