document.addEventListener("DOMContentLoaded", function (event) {
    //Nos conectamos al servicio de socket
    const socket = io("http://10.200.242.225:2000");

    /*
   * Acciones que se realizan cuando se establece conexión con el servidor de socket.
    * tambien introducimos al usuario en la sala que corresponde
   */
    socket.on("connected", (data) => {
        console.log(data.msg);
    });

    /*
     * Acciones que se realizarán cuando otro usuario envia mensaje
     */
    socket.on("", (data) => {
        console.log(data);
    });

    /**
     * Acciones cuando se pulsa el botón de "Enviar"
     */
    document.getElementById("send").addEventListener("submit", (e) => {
        e.preventDefault();
        var msgInput = document.getElementById("userMessage").value;
        var chatBox = document.getElementById("chatText");
        var msg = msgInput.value;

        //Mostramos el mensaje en la ventana para el usuario que lo enviau
        var toSend = { user: "Yo", text: msg };
        console.log("olaa");
        // Definimos el mensaje que vamos a enviar
        socket.emit("broadcast", toSend);
    });
});
