//Load app dependencies
require("dotenv").config();
var express = require('express'),
  mongoose = require('mongoose'),
  path = require('path'),
  http = require('http');
var app = express();
var session = require('express-session');
require("./io");
server = require("http").createServer(app);
const port = 3000;

/*server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});*/

try {
  mongoose.connect(
    `mongodb://root:pass12345@mongodb:27017/users?authSource=admin`,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, res) => {
      if (err) console.log(`ERROR: connecting to Database.  ${err}`);
      else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
  );
} catch (Exception) {
  console.log(Exception);
}

app.use(session({
  resave: false, // don't save session if unmodified
  saveUninitialized: false, // don't create session until something stored
  secret: 'no va xd'
}));

const index = require('./routes/index');
const views = path.join(__dirname + '/views/');
const public = path.join(__dirname + '/public/');

console.log(public);

app.set('view engine', 'html');
app.set('view', views);
app.use(express.urlencoded({ extended: true }));

app.use(express.static(public));


app.use('/', index);
app.listen(port, function () {
  console.log('Example app listening on port ' + port);
});

module.exports = app;

